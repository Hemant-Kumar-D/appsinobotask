package com.example.appsinovotask

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel

import com.example.appsinovotask.databinding.ActivityMainBinding
import com.google.android.material.navigation.NavigationBarView
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity(), NavigationBarView.OnItemSelectedListener {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val toolbar = binding.toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        binding.navigation.setOnItemSelectedListener(this)
        // binding.RecyclerView.layoutManager = LinearLayoutManager(this) binding.RecyclerView.layoutManager= GridLayoutManager(this,3)


        //  val imageSlider = findViewById<ImageSlider>(R.id.imageSlider)
        val imageList = ArrayList<SlideModel>()
        //   list.add(Image("R.drawable.baseline_person_24","R.drawable.baseline_person_24","R.drawable.baseline_person_24"))


        imageList.add(SlideModel(R.drawable.offer, "Offer"))
        imageList.add(SlideModel(R.drawable.offer, "Offer"))
        imageList.add(SlideModel(R.drawable.offer, "Offer"))
        imageList.add(SlideModel(R.drawable.offer, "Offer"))

        binding.imageSlider.setImageList(imageList, ScaleTypes.FIT)

//        binding.search.setOnClickListener {
//            startActivity(Intent(this,BarcodeScannerActivity::class.java))


    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.scanqr -> {
                startActivity(Intent(this, BarcodeScannerActivity::class.java))
            }
        }
            return super.onOptionsItemSelected(item)
        }

}
